package com.example.marvelapp.placeholder

import com.example.marvelapp.R
import java.util.ArrayList
import java.util.HashMap

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 *
 * TODO: Replace all uses of this class before publishing your app.
 */
object PlaceholderContent {

    /**
     * An array of sample (placeholder) items.
     */
    val ITEMS: MutableList<PlaceholderItem> = ArrayList()

    /**
     * A map of sample (placeholder) items, by ID.
     */
    val ITEM_MAP: MutableMap<String, PlaceholderItem> = HashMap()
    val ITEM_TITLES : MutableList<String> = arrayListOf(
        "The Amazing Spider-Man: Spider-Man Wanted!",
        "The Amazing Spider-Man: The Return of an Old Enemy!",
        "The Amazing Spider-Man: The Spider or the Man?",
        "The Amazing Spider-Man: Spidey vs. the Hulk!",
        "The Amazing Spider-Man: The Madness of Mysterio!",
        "The Amazing Spider-Man: The Menace of Mysterio!",
        "The Amazing Spider-Man: Green Goblin Returns",
        "Venom: Part One of Six",
        "Spider-Man Unlimited: Maximum Carnage"
    )
    val ITEM_BACKGROUNDS : MutableList<Int> = arrayListOf(
        R.drawable.spiderman_preview_1,
        R.drawable.spiderman_preview_2,
        R.drawable.spiderman_preview_3,
        R.drawable.spiderman_preview_4,
        R.drawable.spiderman_preview_5,
        R.drawable.spiderman_preview_6,
        R.drawable.spiderman_preview_7,
        R.drawable.spiderman_preview_8,
        R.drawable.spiderman_preview_9
    )
    var ITEM_DETAILS : MutableList<String> = arrayListOf(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pretium sollicitudin tempor. Praesent id nisl eu metus ullamcorper semper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur vulputate massa ante, id ornare lacus consectetur nec. Nullam quis semper erat, vitae consectetur lectus. Ut egestas tellus sit amet leo suscipit consectetur. Nulla nec ipsum eu orci tincidunt scelerisque quis in risus. Morbi rutrum facilisis ligula, quis fringilla mi convallis at.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pretium sollicitudin tempor. Praesent id nisl eu metus ullamcorper semper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur vulputate massa ante, id ornare lacus consectetur nec. Nullam quis semper erat, vitae consectetur lectus. Ut egestas tellus sit amet leo suscipit consectetur. Nulla nec ipsum eu orci tincidunt scelerisque quis in risus. Morbi rutrum facilisis ligula, quis fringilla mi convallis at.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pretium sollicitudin tempor. Praesent id nisl eu metus ullamcorper semper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur vulputate massa ante, id ornare lacus consectetur nec. Nullam quis semper erat, vitae consectetur lectus. Ut egestas tellus sit amet leo suscipit consectetur. Nulla nec ipsum eu orci tincidunt scelerisque quis in risus. Morbi rutrum facilisis ligula, quis fringilla mi convallis at.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pretium sollicitudin tempor. Praesent id nisl eu metus ullamcorper semper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur vulputate massa ante, id ornare lacus consectetur nec. Nullam quis semper erat, vitae consectetur lectus. Ut egestas tellus sit amet leo suscipit consectetur. Nulla nec ipsum eu orci tincidunt scelerisque quis in risus. Morbi rutrum facilisis ligula, quis fringilla mi convallis at.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pretium sollicitudin tempor. Praesent id nisl eu metus ullamcorper semper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur vulputate massa ante, id ornare lacus consectetur nec. Nullam quis semper erat, vitae consectetur lectus. Ut egestas tellus sit amet leo suscipit consectetur. Nulla nec ipsum eu orci tincidunt scelerisque quis in risus. Morbi rutrum facilisis ligula, quis fringilla mi convallis at.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pretium sollicitudin tempor. Praesent id nisl eu metus ullamcorper semper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur vulputate massa ante, id ornare lacus consectetur nec. Nullam quis semper erat, vitae consectetur lectus. Ut egestas tellus sit amet leo suscipit consectetur. Nulla nec ipsum eu orci tincidunt scelerisque quis in risus. Morbi rutrum facilisis ligula, quis fringilla mi convallis at.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pretium sollicitudin tempor. Praesent id nisl eu metus ullamcorper semper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur vulputate massa ante, id ornare lacus consectetur nec. Nullam quis semper erat, vitae consectetur lectus. Ut egestas tellus sit amet leo suscipit consectetur. Nulla nec ipsum eu orci tincidunt scelerisque quis in risus. Morbi rutrum facilisis ligula, quis fringilla mi convallis at.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pretium sollicitudin tempor. Praesent id nisl eu metus ullamcorper semper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur vulputate massa ante, id ornare lacus consectetur nec. Nullam quis semper erat, vitae consectetur lectus. Ut egestas tellus sit amet leo suscipit consectetur. Nulla nec ipsum eu orci tincidunt scelerisque quis in risus. Morbi rutrum facilisis ligula, quis fringilla mi convallis at.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pretium sollicitudin tempor. Praesent id nisl eu metus ullamcorper semper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur vulputate massa ante, id ornare lacus consectetur nec. Nullam quis semper erat, vitae consectetur lectus. Ut egestas tellus sit amet leo suscipit consectetur. Nulla nec ipsum eu orci tincidunt scelerisque quis in risus. Morbi rutrum facilisis ligula, quis fringilla mi convallis at."
    )
    private val COUNT = 9

    init {
        // Add some sample items.
        for (i in 1..COUNT) {
            addItem(createPlaceholderItem(i))
        }
    }

    private fun addItem(item: PlaceholderItem) {
        ITEMS.add(item)
        ITEM_MAP.put(item.id, item)
    }

    private fun createPlaceholderItem(position: Int): PlaceholderItem {
        return PlaceholderItem(ITEM_TITLES[position -1], ITEM_DETAILS[position - 1], ITEM_BACKGROUNDS[position -1]);
    }



    /**
     * A placeholder item representing a piece of content.
     */
    data class PlaceholderItem(val id: String, val details: String, val background: Int) {
        override fun toString(): String = id
    }
}